//
//  Factory.h
//  Cmpe250Project4
//
//  Created by Yiğit Özgümüş on 12/17/14.
//  Copyright (c) 2014 Yigit Ozgumus. All rights reserved.
//

#ifndef __Cmpe250Project4__Factory__
#define __Cmpe250Project4__Factory__

#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <queue>
#include <cstdlib>
#include <algorithm>

//
using AdjList = std::vector<std::list<int> > ;
using EdgeList = std::vector<std::pair<int, int> >;

struct Job{
    // arrival time of the job to the unit
    double arrivalToUnit ;
    //leave time of the job from the unit
    double leaveTime ;
    //trying something risky
};

struct Unit{
    //id of the unit
    int id ;
    
    // Each units' process time of jobs
    double processTime;
    
    bool idle ;
    
    //Unit's counter for queue
    double unitTime ;
    
    // Max number of jobs stacked on their queues
    int max_queue=0;
    
    // Field yet to be understood
    double utilization=0;
    
    std::vector<int> Neighbours;
    
    //Total processing time of the unit
    double busyTime = 0;
    
    //Their queue for the delayed jobs
    std::queue<std::pair<double, int>> JobQueue ;
    
    // Constructor of the unit object
    Unit(double process,int idg){
        processTime = process;
        Neighbours = std::vector<int>();
        unitTime = 0;
        idle =true;
        id = idg;
    }
};

class Factory{
    
public:
    // Random seed for the distribution
    int seed;
    
    // Number of units in the factory
    int units;
    
    // Total number of jobs that units must handle
    int jobs;
    
    // Vector of units-will be used throughout the program
    // chose vector since access is constant time
    std::vector<Unit*> rUnits;
    
    //here it goes
    std::vector <int> JobDest ;
    std::vector<bool> isDone ;
    std::vector<int> lastVisit ;
    
    //total running time of the factory
    double totalTime ;
    
    // Vector of jobs with their arrival times
    std::vector<std::pair<double, int>> Jobs ;
    
    // Connection map of units
    AdjList unitGraph ;
    
    // Middle data structure between adjList and file
    EdgeList unitConnection ;
    
    // Constructor of the factory.Literally
    Factory(std::string fileName);
    
    // Destructor of the factory object
    ~Factory();
    
    //Distribution of the next unit
    void Distribute(Unit* unit,std::pair<double, int> job);
    
    // Builds the adjacency list
    void Build(int size, EdgeList &Edges);
    
    //Random method for the distribution of jobs
    int RandomDist(int branches);
    
    //recursive factory process method
    void ProcessJob(int branch,int job);
};

#endif /* defined(__Cmpe250Project4__Factory__) */
