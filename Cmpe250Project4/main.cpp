//
//  main.cc
//  Cmpe250Project4
//
//  Started by Yiğit Özgümüş on 16/12/14.
//
//#include <iostream>
#include <cstdio>
#include <fstream>
#include <functional>

#include "Factory.h"

int main(int argc, char*argv[]){
    
    //input and output name extraction
    std::string inputFile = argv[1];
    std::string outputFile = argv[2];
    Factory Factory(inputFile);
    
    std::make_heap(Factory.Jobs.begin(),Factory.Jobs.end(),
                   std::greater<std::pair<double, int>>());
    
//    for(auto i : Factory.Jobs)
//       std::cout << i.first << " " << i.second << std::endl ;
    
    while (!Factory.Jobs.empty()) {
        Factory.ProcessJob(Factory.JobDest[Factory.Jobs.front().second], 0);
    }
    
    for(int i = 0 ; i< Factory.units;i++){
        std::cout << Factory.rUnits[i]->JobQueue.size() << std::endl;
    
    }
    
    return 0;
}
