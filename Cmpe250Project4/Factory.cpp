//
//  Factory.cpp
//  Cmpe250Project4
//
//  Created by Yiğit Özgümüş on 12/17/14.
//  Copyright (c) 2014 Yigit Ozgumus. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <functional>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <algorithm>

#include "Factory.h"
void Factory::Build(int size,EdgeList &Edges){
    unitGraph.resize(size);
    for(auto &edge : Edges){
        unitGraph[edge.first].push_front(edge.second);
        rUnits[edge.first]->Neighbours.push_back(edge.second);
    }
}

struct greaterP{
    bool operator()(const std::pair<double,
                    int>& a,const std::pair<double, int>& b) const{
        return a.first > b.first;
    }
};
void Factory::ProcessJob(int branch,int job){
    
    Unit* cUnit = rUnits[branch];
    std::pair<double, int> cJob ;
    cJob = Jobs.front();
    std::pop_heap(Jobs.begin(), Jobs.end(), greaterP());
    Jobs.pop_back();
    if(lastVisit[cJob.second] >= 0){
        rUnits[lastVisit[cJob.second]]->idle = true ;
    }
    
    if(!isDone[cJob.second]){
        if (lastVisit[cJob.second] >= 0 and rUnits[lastVisit[cJob.second]]->idle) {
            Unit* tUnit = rUnits[lastVisit[cJob.second]];
            std::pair<double, int> tJob ;
            if(!tUnit->JobQueue.empty()){
                tJob = tUnit->JobQueue.front();
            tUnit->JobQueue.pop();
            tUnit->idle= false;
            tUnit->unitTime += tUnit->processTime;
            tJob.first = tUnit->unitTime;
            std::cout << tJob.second << "=="<< tJob.first << std::endl;
                Distribute(tUnit, tJob);
                
//            if(tUnit->Neighbours.size() == 1){
//                lastVisit[tJob.second] = (int)tUnit->id;
//                JobDest[tJob.second] = tUnit->Neighbours[0];
//                Jobs.push_back(tJob);
//                std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
//                
//            }else if (tUnit->Neighbours.size() > 1) {
//                lastVisit[tJob.second] = (int)tUnit->id;
//                JobDest[tJob.second]= tUnit->Neighbours[std::floor(((double)std::rand())/RAND_MAX * tUnit->Neighbours.size())];
//                
//                Jobs.push_back(tJob);
//                std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
//                
//            }else {
//                lastVisit[tJob.second] = (int)tUnit->id;
//                isDone[tJob.second] = true ;
//                Jobs.push_back(tJob);
//                std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
//            }
            }

        }
        if(cUnit->idle){
            
            if(cUnit->JobQueue.empty()){
                
                cUnit->idle = false;
                cJob.first += cUnit->processTime;
                cUnit->unitTime = cJob.first;
                std::cout << cJob.second << "=="<< cJob.first << std::endl;
                
            }
            else{
                cUnit->JobQueue.push(cJob);
                cJob = cUnit->JobQueue.front();
                cUnit->JobQueue.pop();
                cUnit->idle = false;
                cUnit->unitTime += cUnit->processTime;
                cJob.first = cUnit->unitTime;
                std::cout << cJob.second << "=="<< cJob.first << std::endl;
            }
            Distribute(cUnit, cJob);
//            
//            if(cUnit->Neighbours.size() == 1){
//                lastVisit[cJob.second] = (int)cUnit->id;
////                Jobs.push_back(cJob);
////                std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
//                JobDest[cJob.second] = cUnit->Neighbours[0];
//                Jobs.push_back(cJob);
//                std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
//                
//            }else if (cUnit->Neighbours.size() > 1) {
//                lastVisit[cJob.second] = (int)cUnit->id;
////                Jobs.push_back(cJob);
////                std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
//                JobDest[cJob.second]= cUnit->Neighbours[std::floor(((double)std::rand())/RAND_MAX * cUnit->Neighbours.size())];
//                
//                Jobs.push_back(cJob);
//                std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
//                
//            }else {
//                if (!isDone[cJob.second]) {
//                    lastVisit[cJob.second] = (int)cUnit->id;
//                    isDone[cJob.second] = true ;
//                    Jobs.push_back(cJob);
//                    std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
//                }
//            }
            
        }else {
            cUnit->JobQueue.push(cJob);
        }
    }
    else{
        
        cUnit = rUnits[lastVisit[cJob.second]] ;
        
        if(!cUnit->JobQueue.empty()){
            cJob = cUnit->JobQueue.front();
            cUnit->JobQueue.pop();
            cUnit->idle = false;
            cUnit->unitTime += cUnit->processTime;
            cJob.first = cUnit->unitTime;
            std::cout << cJob.second << "=="<< cJob.first << std::endl;
            Distribute(cUnit, cJob);
//            if(cUnit->Neighbours.size() == 1){
//                lastVisit[cJob.second] = (int)cUnit->id;
//                JobDest[cJob.second] = cUnit->Neighbours[0];
//                Jobs.push_back(cJob);
//                std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
//                
//            }else if (cUnit->Neighbours.size() > 1) {
//                lastVisit[cJob.second] = (int)cUnit->id;
//                JobDest[cJob.second]= cUnit->Neighbours[std::floor(((double)std::rand())/RAND_MAX * cUnit->Neighbours.size())];
//                
//                Jobs.push_back(cJob);
//                std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
//                
//            }else {
//                if (!isDone[cJob.second]) {
//                    lastVisit[cJob.second] = (int)cUnit->id;
//                    isDone[cJob.second] = true ;
//                    Jobs.push_back(cJob);
//                    std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
//                }
//            }
        }
    }
}
void Factory::Distribute(Unit *unit, std::pair<double, int> job){
    if(unit->Neighbours.size() == 1){
        lastVisit[job.second] = (int)unit->id;
        JobDest[job.second] = unit->Neighbours[0];
        Jobs.push_back(job);
        std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
        
    }else if (unit->Neighbours.size() > 1) {
        lastVisit[job.second] = (int)unit->id;
        JobDest[job.second]= unit->Neighbours[std::floor(((double)std::rand())/RAND_MAX * unit->Neighbours.size())];
        
        Jobs.push_back(job);
        std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
        
    }else {
        if (!isDone[job.second]) {
            lastVisit[job.second] = (int)unit->id;
            isDone[job.second] = true ;
            Jobs.push_back(job);
            std::push_heap(Jobs.begin(),Jobs.end(),greaterP());
        }
    }
}

Factory::Factory(std::string fileName){
    
    //initialize the fstream for the file
    std::ifstream inFile(fileName);
    
    // take the fields
    // std::srand(std::time(0));
    inFile >> units ;
    inFile >> std::ws;
    rUnits = std::vector<Unit*>();
    
    // for loop for the units both process time and possible connections
    std::string unitGet ;
    int source,target ; double process ;
    for(int unit=0;unit<units;unit++){
        inFile >> source ;
        inFile >> process;
        
        // creating a vector of units to keep track of them
        rUnits.push_back(new Unit(process,unit));
        while(inFile.get() == ' '){
            inFile >> target;
            unitConnection.push_back({source,target});
        }
    }
    //Build the connections using Adjacency list
    Build(units,unitConnection);
    //debug printing
    /*
     int nodelabel = 0;
     for(auto& nodes_list : unitGraph){
     std::cout<< nodelabel++ <<" -> ";
     for(int node : nodes_list){
     std::cout<<node<<" ";
     }
     std::cout<<std::endl;
     }
     */
    inFile >> jobs ;
    
    Jobs = std::vector<std::pair<double, int>>(jobs);
    
    
    //vector of job objects, could be changed to queue
    double arrival;
    for(int index=0;index<jobs ;index++){
        inFile>> arrival ;
        Jobs[index].first = arrival ;
        Jobs[index].second = index ;
    }
    totalTime = 0 ;
    JobDest = std::vector<int>(jobs,0);
    isDone = std::vector<bool>(jobs,false);
    lastVisit = std::vector<int>(jobs,-1);
    std::make_heap(Jobs.begin(), Jobs.end(), greaterP());
}

Factory::~Factory(){
    for(int i=0;i<rUnits.size();i++) delete rUnits[i];
}
